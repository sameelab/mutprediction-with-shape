#!/bin/bash
# Convenient script to setup directories if you haven't done so

makedir() {
  # Function to set up a directory if it doesn't already exist
  local newdir=$1
  if [ ! -d $newdir ]; then mkdir -pv $newdir; fi
}


# Set up a few dirs
makedir data_input/
makedir data_intermediate/
makedir data_extras/
makedir manuscript/figures/


# Move notebooks from Notebook/ to current working directory
cp Notebooks/* .

