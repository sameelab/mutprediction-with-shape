## data_out directory
### Zian Liu
### Last updated: 7/22/2021

As the name suggests, this is the default directory for data outputs.

We have included a table here that documents the mutation rate predictions for all 7-mer single nucleotide substitution patterns from our best performing model, please feel free to use this table directly.
