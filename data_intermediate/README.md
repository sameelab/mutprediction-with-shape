## data_intermediate directory

As the name suggests, this is the directory where intermediate data is stored.

We have included a few pre-built L1-selection results here for reproduceability purposes packaged into a .tar.gz archive. We also recommend building these yourself by following the example pipeline.
