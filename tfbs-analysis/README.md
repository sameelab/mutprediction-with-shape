# Motif processing
### Zian Liu
### Last updated: 6/8/2023


As stated in the main README: 
The motifs are downloaded from compbio.mit.edu/encode-motifs/. The webpage appears to be dysfunctional
at the moment, so please contact us if you want to do something with this data but couldn't; we will 
try our best to accommodate.

Since this job was originally run on a cluster using the PBS job scheduling system, our main script is
pbs_pipeline_tfbs.pbs.
You can modify it to fit your cluster's job scheduling system, just don't forget to change the variables. 

Step 5 is something we tried to do originally for a control data but was bugged, so please skip it.

For the other scripts:
* script_fasta2shape.R: this is for generating the DNA shape data.
* script_TFBS_concatenate.py: this is for data formatting, currently not used by the pbs script.
